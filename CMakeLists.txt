# Project Settings. ------------------------------------------------------------
cmake_minimum_required(VERSION 3.7.2)
project(project_name)
set (CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0")

find_package(PkgConfig REQUIRED)
find_package(Threads REQUIRED)

# Ensures that we do an out of source build. -----------------------------------

MACRO(MACRO_ENSURE_OUT_OF_SOURCE_BUILD MSG)
     STRING(COMPARE EQUAL "${CMAKE_SOURCE_DIR}"
     "${CMAKE_BINARY_DIR}" insource)
     GET_FILENAME_COMPONENT(PARENTDIR ${CMAKE_SOURCE_DIR} PATH)
     STRING(COMPARE EQUAL "${CMAKE_SOURCE_DIR}"
     "${PARENTDIR}" insourcesubdir)
    IF(insource OR insourcesubdir)
        MESSAGE(FATAL_ERROR "${MSG}")
    ENDIF(insource OR insourcesubdir)
ENDMACRO(MACRO_ENSURE_OUT_OF_SOURCE_BUILD)

MACRO_ENSURE_OUT_OF_SOURCE_BUILD(
    "${CMAKE_PROJECT_NAME} requires an out of source build."
)

# Project Include directories. -------------------------------------------------
include_directories(include libs)

# Build and link main executable. ----------------------------------------------
add_executable(
  ${CMAKE_PROJECT_NAME}
  ./src/main.cpp
)

# Build and link the test runner. ----------------------------------------------
add_executable(test_runner ./test/test_main.cpp
                           ./test/test_test.cpp)
