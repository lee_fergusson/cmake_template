# Custom CMake Project Template

## Getting Started

Firstly rename your project at the top of the `./CMakeLists.txt` file.

```cmake
# Project Settings.
cmake_minimum_required(VERSION 3.7.2)
project(my_project)
```

It is recommended to explicitly specify all `.cpp` files to be included in the build instead of relying on file globs.

```cmake
# Build and link main executable. ---------------------------------------------
add_executable(${CMAKE_PROJECT_NAME}
  ./src/main.cpp
  ./src/foo.cpp
  ./src/bar.cpp
)
```

## Source Control

It is a good idea to remove the remote this template originated from.

```bash
git remote remove origin
```

Alternativly you may wish to start you project with a fresh git history, the easiest way to acheive this is to delete the project's ```.git``` directory and reinitialze the repository.

```bash
# From the projects' root directory
rm -rf .git
git init
git add -A
git commit -m "Initial commit"
```

## Build Instructions

You should be performing 'out of source' builds, the recommentded place to do so is within a ```./build``` directory located within the root project directory. (This directory will be ignored by source control by default)

```bash
# Create a folder to hold the build files.
mkdir build

# Move into to the build directory.
cd build

# Generate CMake build scripts.
cmake ..

# The project can then be built using standard the make tool.
make
```

## Testing

This project template uses [Catch2](https://github.com/catchorg/Catch2) as it's default testing framework. You might also want to replace the `./libs/catch2/catch.hpp` file with the latest version from the repository, in case this template hasn't been updated recently.

Performing a build will produce a `./build/test_runner` executable for runnning your test suite.

Running this execuatable should result in the default output below.

```bash
$ ./test_runner
===============================================================================
All tests passed (1 assertion in 1 test case)
```
